const http = require("http");
http.createServer(function(request, response) {

	// Homepage
	if (request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}

	// Profile
	if (request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile!');
	}

	// Courses
	if (request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our courses available:");
	}

	// Add Course
	if (request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add a course to our resources');
	}

	// Update Course
	if (request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Update a course to our resources");
	}

	// Archive Courses
	if (request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
	}

}).listen(4000);
console.log('Server is running at localhost:4000')